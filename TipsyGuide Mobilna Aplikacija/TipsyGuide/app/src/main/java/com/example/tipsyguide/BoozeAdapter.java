package com.example.tipsyguide;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.FieldPosition;
import java.util.List;

public class BoozeAdapter extends RecyclerView.Adapter<NameViewHolder>{

    private List<Drink> drinkList;

    public BoozeAdapter(List<Drink> drinkList){
        this.drinkList = drinkList;

    }

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_cell,parent,false);
        return new NameViewHolder(listItemView);
    }



    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        holder.setDrink(drinkList.get(position));
    }

    @Override
    public int getItemCount() {
        return drinkList.size();
    }

    public String OpenActivity(int position){
        String name = drinkList.get(position).getName();
        Log.d("KLIK", name);
        //POŠALJI OVDJE NAME U INTENT PREKO PUTEXTRA
        return name;
    }


}
