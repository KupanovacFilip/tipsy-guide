package com.example.tipsyguide;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {

    //Nakon ovoga još svaki element koktela iz recycler viewa inicijaliziraj
    private final TextView nameTextView;
    private final TextView descTextView;
    private final ImageView imageView;

    public NameViewHolder(@NonNull View itemView/*, CustomClickListener nameClickListener*/) {
        super(itemView);
        nameTextView = itemView.findViewById(R.id.tvNameDrinkCell);
        descTextView = itemView.findViewById(R.id.tvDescDrinkCell);
        imageView = itemView.findViewById(R.id.ivDrinkCell);
        initializeCell();

        itemView.setOnClickListener(this);

    }

    private void initializeCell() {
        descTextView.setText("Generic text");
        //nije potrebno kada se do kraja riješi setDrink
    }

    public void setDrink(Drink drink) {
        //Probaj ovdje postaviti svaki element
        nameTextView.setText(drink.getName());
        setItems();
        //postaviti još textView opisa te sliku kad skupiš resurse
    }

    @Override
    public void onClick(View v) {
        Context c = v.getContext();
        Intent intent;
        if(checkForBooze()){
            intent = new Intent(v.getContext(), ZestaActivity.class);
        }
        else{
            intent = new Intent(v.getContext(), CocktailActivity.class);
        }
        intent.putExtra("NAME_KEY", nameTextView.getText());

        c.startActivity(intent);


    }

    private boolean checkForBooze() {
        boolean rum,vodka,gin,tequila;
        rum = nameTextView.getText().toString()=="Rum";
        vodka = nameTextView.getText().toString()=="Vodka";
        gin = nameTextView.getText().toString()=="Gin";
        tequila = nameTextView.getText().toString()=="Tequila";
        return (rum || vodka || gin || tequila);
    }

    public void setItems(){
        String aux = nameTextView.getText().toString();
        switch (aux){
            case "Cosmopolitan":
                imageView.setImageResource(R.drawable.cosmopolitan_1x1);
                descTextView.setText(R.string.cosmopolitanInfo);
                break;
            case "Sex on the Beach":
                imageView.setImageResource(R.drawable.sex_on_the_beach_1x1);
                descTextView.setText(R.string.sexOnTheBeachInfo);
                break;
            case "Blue Lagoon":
                imageView.setImageResource(R.drawable.blue_lagoon_1x1);
                descTextView.setText(R.string.blueLagoonInfo);
                break;
            case "Pina Colada":
                imageView.setImageResource(R.drawable.pina_colada_1x1);
                descTextView.setText(R.string.pinaColadaInfo);
                break;
            case "Cuba Libre":
                imageView.setImageResource(R.drawable.cuba_libre_1x1);
                descTextView.setText(R.string.cubaLibreInfo);
                break;
            case "Paloma":
                imageView.setImageResource(R.drawable.paloma_1x1);
                descTextView.setText(R.string.palomaInfo);
                break;
            case "Tequila Sunrise":
                imageView.setImageResource(R.drawable.tequila_sunrise_1x1);
                descTextView.setText(R.string.tequilaSunriseInfo);
                break;
            case "Mai Tai":
                imageView.setImageResource(R.drawable.mai_tai_1x1);
                descTextView.setText(R.string.maiTaiInfo);
                break;
            case "Sunset Daiquiri":
                imageView.setImageResource(R.drawable.sunset_daiquiri_1x1);
                descTextView.setText(R.string.sunsetDaiquiriInfo);
                break;
            case "Negroni":
                imageView.setImageResource(R.drawable.negroni_1x1);
                descTextView.setText(R.string.negroniInfo);
                break;
            case "Bella Ciao":
                imageView.setImageResource(R.drawable.bella_ciao_1x1);
                descTextView.setText(R.string.bellaCiaoInfo);
                break;
            case "Bramble":
                imageView.setImageResource(R.drawable.bramble_1x1);
                descTextView.setText(R.string.brambleInfo);
                break;
            case "Rum":
                imageView.setImageResource(R.drawable.rum_1x1);
                descTextView.setText(R.string.rumCell);
                break;
            case "Gin":
                imageView.setImageResource(R.drawable.gin_1x1);
                descTextView.setText(R.string.ginCell);
                break;
            case "Vodka":
                imageView.setImageResource(R.drawable.vodka_1x1);
                descTextView.setText(R.string.vodkaCell);
                break;
            case "Tequila":
                imageView.setImageResource(R.drawable.tequila_1x1);
                descTextView.setText(R.string.tequilaCell);
                break;
            default:
                imageView.setImageResource(R.drawable.recycler_cell_border_white);
        }
    }
}
