package com.example.tipsyguide;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ZestaActivity extends AppCompatActivity {
    private TextView mNameTextView;
    private ImageView mImageView;
    private Button btnHome;
    private TextView mBoozeInfo;

    private RecyclerView recyclerView;
    private List<Drink> cocktailList;
    private CocktailAdapter cocktailAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zesta);
        mNameTextView = findViewById(R.id.tvBoozeName);
        mImageView = findViewById(R.id.ivBoozePic);
        mBoozeInfo = findViewById(R.id.tvAboutBooze);

        btnHome = findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        Bundle bundle = getIntent().getExtras();

        if(bundle == null){
            mNameTextView.setText(null);
        }else{
            mNameTextView.setText(bundle.getString("NAME_KEY"));
            setItems();
        }
        setupCocktails();
        recyclerView = findViewById(R.id.boozeRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this)); //ZBOG OVOG MOŽDA NE RADI

        cocktailAdapter = new CocktailAdapter(cocktailList);
        recyclerView.setAdapter(cocktailAdapter);
    }

    private void setupCocktails() {
        cocktailList = new ArrayList<>();
        switch (mNameTextView.getText().toString()){
            case "Vodka":
                cocktailList.add(new Drink("Cosmopolitan"));
                cocktailList.add(new Drink("Sex on the Beach"));
                cocktailList.add(new Drink("Blue Lagoon"));
                break;
            case "Gin":
                cocktailList.add(new Drink("Negroni"));
                cocktailList.add(new Drink("Bella Ciao"));
                cocktailList.add(new Drink("Bramble"));
                break;
            case "Tequila":
                cocktailList.add(new Drink("Tequila Sunrise"));
                cocktailList.add(new Drink("Paloma"));
                break;
            case "Rum":
                cocktailList.add(new Drink("Pina Colada"));
                cocktailList.add(new Drink("Cuba Libre"));
                cocktailList.add(new Drink("Mai Tai"));
                cocktailList.add(new Drink("Sunset Daiquiri"));
                break;
            default:    break;
        }
    }


    private void setItems() {
        String aux = mNameTextView.getText().toString();
        switch (aux){
            case "Rum":
                mImageView.setImageResource(R.drawable.rum_og);
                mBoozeInfo.setText(R.string.rumInfo);
                break;
            case "Gin":
                mImageView.setImageResource(R.drawable.gin_og);
                mBoozeInfo.setText(R.string.ginInfo);
                break;
            case "Vodka":
                mImageView.setImageResource(R.drawable.vodka_og);
                mBoozeInfo.setText(R.string.vodkaInfo);
                break;
            case "Tequila":
                mImageView.setImageResource(R.drawable.tequila_og);
                mBoozeInfo.setText(R.string.tequilaInfo);
                break;
            default:
                mImageView.setImageResource(R.drawable.recycler_cell_border_white);
        }
    }
}
