package com.example.tipsyguide;

import android.util.Log;
import android.view.ViewGroup;
import android.view.View;
import android.view.LayoutInflater;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CocktailAdapter extends RecyclerView.Adapter<NameViewHolder> {
    private static final  String TAG ="CustomAdapter";
    private List<Drink> drinkList;


    public CocktailAdapter(List<Drink> cocktailList){
        this.drinkList = cocktailList;


    }

    @NonNull
    @Override
    public NameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listItemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_cell,parent,false);
        return new NameViewHolder(listItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NameViewHolder holder, int position) {
        holder.setDrink(drinkList.get(position));
    }

    @Override
    public int getItemCount() {
        return drinkList.size();
    }

    public String OpenActivity(int position){
        String name = drinkList.get(position).getName();
        Log.d("KLIK", name);
        return name;
    }

}
