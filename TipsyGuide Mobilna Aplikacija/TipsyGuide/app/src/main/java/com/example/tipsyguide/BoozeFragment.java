package com.example.tipsyguide;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class BoozeFragment extends Fragment{
    private RecyclerView recyclerView;
    private List<Drink> drinkList;
    private BoozeAdapter boozeAdapter;
    private TextView tvFragmentInfo;

    public static BoozeFragment newInstance(){
        BoozeFragment boozeFragment = new BoozeFragment();
        return boozeFragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupList();
        setupRecyclerView(view);

        tvFragmentInfo = view.findViewById(R.id.tvFragmentInfo);
        tvFragmentInfo.setVisibility(View.GONE);
    }


    private void setupRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        boozeAdapter = new BoozeAdapter(drinkList);
        recyclerView.setAdapter(boozeAdapter);
    }

    private void setupList(){
        drinkList = new ArrayList<>();

        drinkList.add(new Drink("Vodka"));
        drinkList.add(new Drink("Rum"));
        drinkList.add(new Drink("Tequila"));
        drinkList.add(new Drink("Gin"));

    }


}
