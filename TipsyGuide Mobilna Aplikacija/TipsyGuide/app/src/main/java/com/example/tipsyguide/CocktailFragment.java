package com.example.tipsyguide;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class CocktailFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<Drink> cocktailList;
    private CocktailAdapter cocktailAdapter;
    //dodati po potrebi ostale elemente

    public static CocktailFragment newInstance(){
        CocktailFragment cocktailFragment = new CocktailFragment();
        return cocktailFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_slide, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupCocktails();
        setupRecyclerView(view);

    }

    private void setupRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        cocktailAdapter = new CocktailAdapter(cocktailList);
        recyclerView.setAdapter(cocktailAdapter);

    }

    private void setupCocktails() {
        cocktailList = new ArrayList<>();
        cocktailList.add(new Drink("Cosmopolitan"));
        cocktailList.add(new Drink("Sex on the Beach"));
        cocktailList.add(new Drink("Blue Lagoon"));
        cocktailList.add(new Drink("Pina Colada"));
        cocktailList.add(new Drink("Cuba Libre"));
        cocktailList.add(new Drink("Paloma"));
        cocktailList.add(new Drink("Tequila Sunrise"));
        cocktailList.add(new Drink("Mai Tai"));
        cocktailList.add(new Drink("Sunset Daiquiri"));
        cocktailList.add(new Drink("Negroni"));
        cocktailList.add(new Drink("Bella Ciao"));
        cocktailList.add(new Drink("Bramble"));
    }

}
