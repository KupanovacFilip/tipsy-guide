package com.example.tipsyguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class CocktailActivity extends AppCompatActivity {
    private TextView mNameTextView;
    private ImageView mImageView;
    private TextView mIngredientsTextViev;
    private TextView mRecipeTextView;

    private Button btnHome;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.koktel);
        mNameTextView = findViewById(R.id.tvCocktailName);
        mImageView = findViewById(R.id.ivCocktailPic);
        mIngredientsTextViev = findViewById(R.id.tvIngredientsList);
        mRecipeTextView = findViewById(R.id.tvRecipe);
        btnHome = findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        Bundle bundle = getIntent().getExtras();

        if(bundle == null){
            mNameTextView.setText(null);
        }else{
            mNameTextView.setText(bundle.getString("NAME_KEY"));
            setItems();
            setIngredients(mIngredientsTextViev);
            setRecipe(mRecipeTextView);
        }
    }

    public void setItems(){
        String aux = mNameTextView.getText().toString();
        switch (aux){
            case "Cosmopolitan":
                mImageView.setImageResource(R.drawable.cosmopolitan_og);
                mIngredientsTextViev.setText(R.string.cosmopolitanIngredients);
                mRecipeTextView.setText(R.string.cosmopolitanRecipe);
                break;
            case "Sex on the Beach":
                mImageView.setImageResource(R.drawable.sex_on_the_beach_og);
                mIngredientsTextViev.setText(R.string.sexOnTheBeachIngredients);
                mRecipeTextView.setText(R.string.sexOnTheBeachRecipe);
                break;
            case "Blue Lagoon":
                mImageView.setImageResource(R.drawable.blue_lagoon_og);
                mIngredientsTextViev.setText(R.string.blueLagoonIngredients);
                mRecipeTextView.setText(R.string.blueLagoonRecipe);
                break;
            case "Pina Colada":
                mImageView.setImageResource(R.drawable.pina_colada_og);
                mIngredientsTextViev.setText(R.string.pinaColadaIngredients);
                mRecipeTextView.setText(R.string.pinaColadaRecipe);
                break;
            case "Cuba Libre":
                mImageView.setImageResource(R.drawable.cuba_libre_og);
                mIngredientsTextViev.setText(R.string.cubaLibreIngredients);
                mRecipeTextView.setText(R.string.cubaLibreRecipe);
                break;
            case "Paloma":
                mImageView.setImageResource(R.drawable.paloma_og);
                mIngredientsTextViev.setText(R.string.palomaIngredients);
                mRecipeTextView.setText(R.string.palomaRecipe);
                break;
            case "Tequila Sunrise":
                mImageView.setImageResource(R.drawable.tequila_sunrise_og);
                mIngredientsTextViev.setText(R.string.tequilaSunriseIngredients);
                mRecipeTextView.setText(R.string.tequilaSunriseRecipe);
                break;
            case "Mai Tai":
                mImageView.setImageResource(R.drawable.mai_tai_og);
                mIngredientsTextViev.setText(R.string.maiTaiIngredients);
                mRecipeTextView.setText(R.string.maiTaiRecipe);
                break;
            case "Sunset Daiquiri":
                mImageView.setImageResource(R.drawable.sunset_daiquiri_og);
                mIngredientsTextViev.setText(R.string.sunsetDaiquiriIngredients);
                mRecipeTextView.setText(R.string.sunsetDaiquiriRecipe);
                break;
            case "Negroni":
                mImageView.setImageResource(R.drawable.negroni_og);
                mIngredientsTextViev.setText(R.string.negroniIngredients);
                mRecipeTextView.setText(R.string.negroniRecipe);
                break;
            case "Bella Ciao":
                mImageView.setImageResource(R.drawable.bella_ciao_og);
                mIngredientsTextViev.setText(R.string.bellaCiaoIngredients);
                mRecipeTextView.setText(R.string.bellaCiaoRecipe);
                break;
            case "Bramble":
                mImageView.setImageResource(R.drawable.bramble_og);
                mIngredientsTextViev.setText(R.string.brambleIngredients);
                mRecipeTextView.setText(R.string.brambleRecipe);
                break;
            default:
                mImageView.setImageResource(R.drawable.recycler_cell_border_white);
        }
    }

    private void setIngredients(TextView mIngredientsTextViev) {
        String ingredients;
        ingredients = "Sastojci:\n" + mIngredientsTextViev.getText().toString();
        mIngredientsTextViev.setText(ingredients);
    }
    private void setRecipe(TextView recipeTextView) {
        String recipe;
        recipe = "Priprema:\n" + recipeTextView.getText().toString();
        recipeTextView.setText(recipe);
    }
}
